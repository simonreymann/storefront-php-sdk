# About You Cloud Storefront API PHP SDK

## Requirements

PHP 5.6.0 and later.

## Composer

You can install the SDK via Composer. Run the following command:

`composer require aboutyou/storefront-api`

## Documentation

See the [PHP SDK docs](https://resources.aboutyou.cloud/en/dev/storefront/introduction).

